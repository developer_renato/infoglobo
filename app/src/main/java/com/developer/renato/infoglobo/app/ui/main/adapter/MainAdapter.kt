package com.developer.renato.infoglobo.app.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.developer.renato.infoglobo.R
import com.developer.renato.infoglobo.databinding.AdapterMainListItemBinding
import com.developer.renato.infoglobo.domain.model.Conteudo
import com.google.gson.Gson
import com.squareup.picasso.Picasso

class MainAdapter(private val conteudos: List<Conteudo>, onItemClickListener : OnItemClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val onItemClickListener : OnItemClickListener = onItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MainViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_main_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val binding = (holder as MainViewHolder).binding
        val conteudo = conteudos[position]
        load(binding, conteudo)
        binding.conteudo = conteudo

        holder.itemView.setOnClickListener { view ->
            onItemClickListener.onItemClicked(Gson().toJson(conteudo))
        }
    }

    private fun load(binding: AdapterMainListItemBinding, conteudo: Conteudo) {
        binding.titulo.text = conteudo.titulo
        var url: String =  ""
        if (conteudo.imagens!=null && conteudo.imagens!!.size>0){
            url = conteudo.imagens?.get(0)?.url.toString()
            Picasso.get()
                .load(url)
                .placeholder(R.drawable.imageholder_error)
                .error(R.drawable.imageholder_error)
                .into(binding.image)
        }
    }

    override fun getItemCount() = conteudos.size

}

class MainViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val binding: AdapterMainListItemBinding = AdapterMainListItemBinding.bind(view)
}

interface OnItemClickListener{
    fun onItemClicked(gson: String)
}