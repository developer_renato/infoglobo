package com.developer.renato.infoglobo.app.config

import com.developer.renato.infoglobo.app.ui.main.MainViewModel
import com.developer.renato.infoglobo.domain.repository.NoticiasRepository
import com.developer.renato.infoglobo.resources.remote.api.NoticiasApi
import com.developer.renato.infoglobo.resources.repository.NoticiasDataRepository

import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module


val uiModule = module {
    viewModel { MainViewModel(get()) }
}

val repositoryModule = module {
    single<NoticiasRepository> { NoticiasDataRepository(get()) }
}

val remoteModule = module {
    single { provideOkHttpClient() }
    single { provideRetrofit(get()) }
    single { createApi<NoticiasApi>(get()) }
}
