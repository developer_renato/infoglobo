package com.developer.renato.infoglobo.domain.model

class Conteudo {
     var autores: List<String>? = null
     var informePublicitario: Boolean? = null
     var  subTitulo: String? = null
     var texto: String? = null
     var videos: List<Any>? = null
     var atualizadoEm: String? = null
     var id: Int? = null
     var publicadoEm: String? = null
     var secao: Secao? = null
     var tipo: String? = null
     var titulo: String? = null
     var url: String? = null
     var urlOriginal: String? = null
     var imagens: List<Imagen>? = null
}