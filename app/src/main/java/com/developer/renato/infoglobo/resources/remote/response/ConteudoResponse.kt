package com.developer.renato.infoglobo.resources.remote.response

import com.developer.renato.infoglobo.domain.model.Imagen
import com.developer.renato.infoglobo.domain.model.Secao
import com.squareup.moshi.Json

class ConteudoResponse {
    @Json(name = "autores")
    private val autores: List<String>? = null

    @Json(name = "informePublicitario")
    private val informePublicitario: Boolean? = null

    @Json(name = "subTitulo")
    private val subTitulo: String? = null

    @Json(name = "texto")
    private val texto: String? = null

    @Json(name = "videos")
    private val videos: List<Any>? = null

    @Json(name = "atualizadoEm")
    private val atualizadoEm: String? = null

    @Json(name = "id")
    private val id: Int? = null

    @Json(name = "publicadoEm")
    private val publicadoEm: String? = null

    @Json(name = "secao")
    private val secao: Secao? = null

    @Json(name = "tipo")
    private val tipo: String? = null

    @Json(name = "titulo")
    private val titulo: String? = null

    @Json(name = "url")
    private val url: String? = null

    @Json(name = "urlOriginal")
    private val urlOriginal: String? = null

    @Json(name = "imagens")
    private val imagens: List<Imagen>? = null
}