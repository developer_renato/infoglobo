package com.developer.renato.infoglobo.resources.repository

import com.developer.renato.infoglobo.domain.repository.NoticiasRepository
import com.developer.renato.infoglobo.resources.remote.api.NoticiasApi
import com.developer.renato.infoglobo.resources.remote.response.toModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NoticiasDataRepository (private val api: NoticiasApi) : NoticiasRepository {

    override suspend fun getAll() = withContext(Dispatchers.IO) {
        api.getAll().await().map { it.toModel() }
    }

}