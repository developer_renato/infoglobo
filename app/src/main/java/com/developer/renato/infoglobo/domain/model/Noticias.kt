package com.developer.renato.infoglobo.domain.model

data class Noticias(
     var conteudos: List<Conteudo>,
     var produto: String
)