package com.developer.renato.infoglobo.app.ui.splash.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.developer.renato.infoglobo.R
import com.developer.renato.infoglobo.app.ui.main.activity.NoticiaActivity

class SplashActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.requestFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.splash)

        Handler().postDelayed({
            startActivity(Intent(this@SplashActivity, NoticiaActivity::class.java))
            finish()
        },4000)
    }
}