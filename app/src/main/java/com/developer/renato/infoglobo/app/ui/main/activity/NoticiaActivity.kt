package com.developer.renato.infoglobo.app.ui.main.activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.developer.renato.infoglobo.R
import com.developer.renato.infoglobo.app.ui.details.activity.DetailsActivity
import com.developer.renato.infoglobo.app.ui.main.MainViewModel
import com.developer.renato.infoglobo.app.ui.main.adapter.BannerPagerAdapter
import com.developer.renato.infoglobo.app.ui.main.adapter.MainAdapter
import com.developer.renato.infoglobo.app.ui.main.adapter.OnItemClickListener
import com.google.android.material.tabs.TabLayoutMediator
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.developer.renato.infoglobo.databinding.NoticiasBinding as Binding

class NoticiaActivity : AppCompatActivity(), OnItemClickListener {
    private lateinit var viewManager: RecyclerView.LayoutManager

    private val viewModel by viewModel<MainViewModel>()
    val binding by lazy {
        DataBindingUtil.setContentView<Binding>(this, R.layout.noticias)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getAll()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.viewModel = viewModel
        binding.viewPager.setPadding(0, 0, 0, 0);
        binding.recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        observeLoading()
        observeNoticias()
        observeError()
    }

    private fun observeLoading() {
        viewModel.loading.observe(
            this,
            Observer { t ->
                if(!t){
                    if (binding.progressBar != null) {
                        binding.progressBar.visibility = View.INVISIBLE
                        binding.swipeContainer.isRefreshing = false
                    }
                }
            })
    }

    private fun observeNoticias() {
        viewModel.noticias.observe(this,
            Observer { noticias ->

                binding.viewPager.adapter = BannerPagerAdapter(noticias.get(0).conteudos)
                TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
                }.attach()

                binding.recyclerView.apply {
                    setHasFixedSize(true)
                    adapter = MainAdapter(noticias.get(0).conteudos,  this@NoticiaActivity)
                    layoutManager = viewManager
                    addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
                }
            }
        )
    }
    private fun observeError() {
        viewModel.error.observe(
            this,
            Observer { t ->
                Log.e("LOAD_NOTICIAS", "$t")
            })
    }
    override fun onItemClicked(gson: String) {
        viewModel.setMsgCommunicator(gson)
        startActivity(Intent(this@NoticiaActivity, DetailsActivity::class.java).putExtra(resources.getString(R.string.key), gson))
    }
}