package com.developer.renato.infoglobo.app.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.developer.renato.infoglobo.R
import com.developer.renato.infoglobo.databinding.AdapterBannerBinding
import com.developer.renato.infoglobo.domain.model.Conteudo
import com.squareup.picasso.Picasso

class BannerPagerAdapter(private val conteudos: List<Conteudo>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return BannerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_banner, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val binding = (holder as BannerViewHolder).binding
        val conteudo = conteudos[position]
        load(binding, conteudo)
        binding.conteudo = conteudo
    }

    private fun load(binding: AdapterBannerBinding, conteudo: Conteudo) {
        binding.textViewTitulo.text = conteudo.titulo
        binding.textViewSecaoNome.text = conteudo.secao?.nome
        var url: String =  ""
        if (conteudo.imagens!=null && conteudo.imagens!!.size>0){
            url = conteudo.imagens?.get(0)?.url.toString()
            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.imageholder)
                    .error(R.drawable.imageholder_error)
                    .into(binding.imageView)
        }
    }

    override fun getItemCount() = conteudos.size
}

class BannerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val binding: AdapterBannerBinding = AdapterBannerBinding.bind(view)
}