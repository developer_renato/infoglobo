package com.developer.renato.infoglobo.resources.remote.response

import com.squareup.moshi.Json

class SecaoResponse {
    @Json(name = "nome")
    private val nome: String? = null

    @Json(name = "url")
    private val url: String? = null
}