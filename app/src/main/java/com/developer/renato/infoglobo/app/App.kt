package com.developer.renato.infoglobo.app

import android.app.Application
import com.developer.renato.infoglobo.app.config.remoteModule
import com.developer.renato.infoglobo.app.config.repositoryModule
import com.developer.renato.infoglobo.app.config.uiModule
import org.koin.android.ext.android.startKoin


open class App : Application() {

    private val appModules by lazy {
        listOf(remoteModule, repositoryModule, uiModule)
    }

    override fun onCreate() {
        super.onCreate()
        startKoin(this, appModules)
    }

}