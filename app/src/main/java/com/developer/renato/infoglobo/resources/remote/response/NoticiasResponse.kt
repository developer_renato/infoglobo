package com.developer.renato.infoglobo.resources.remote.response

import com.developer.renato.infoglobo.domain.model.Conteudo
import com.developer.renato.infoglobo.domain.model.Noticias
import com.squareup.moshi.Json

data class NoticiasResponse(
    @Json(name = "conteudos")
    val conteudos: List<Conteudo> = listOf(),

    @Json(name = "produto")
    val produto: String
)

fun NoticiasResponse.toModel() = Noticias(this.conteudos, this.produto)
