package com.developer.renato.infoglobo.domain.model

class Imagen {
    var autor: String? = null
    var fonte: String? = null
    var legenda: String? = null
    var url: String? = null
}