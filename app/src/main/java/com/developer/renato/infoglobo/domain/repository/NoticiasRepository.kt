package com.developer.renato.infoglobo.domain.repository

import com.developer.renato.infoglobo.domain.model.Noticias

interface NoticiasRepository {
    suspend fun getAll(): List<Noticias>
}