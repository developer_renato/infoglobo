package com.developer.renato.infoglobo.app.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.developer.renato.infoglobo.domain.model.Noticias
import com.developer.renato.infoglobo.domain.repository.NoticiasRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class MainViewModel(private val repository: NoticiasRepository) : ViewModel() {

    private val viewModelJob = SupervisorJob()

    private val viewModeScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val _noticias: MutableLiveData<List<Noticias>> = MutableLiveData()

    private val _loading: MutableLiveData<Boolean> = MutableLiveData()

    private val _error: MutableLiveData<Throwable> = MutableLiveData()

    val noticias: LiveData<List<Noticias>> get() = _noticias

    val loading: LiveData<Boolean> get() = _loading

    val error: LiveData<Throwable> get() = _error

    fun getAll() {
        viewModeScope.launch {
            _loading.value = true
            try {
                _noticias.value = repository.getAll()
                _loading.value = false
            } catch(t: Throwable) {
                _noticias.value = emptyList()
                _error.value = t
            } finally {
                _loading.value = false
            }
        }
    }
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    val message =MutableLiveData<Any>()
    fun setMsgCommunicator(msg:String){
        message.setValue(msg)
    }

}