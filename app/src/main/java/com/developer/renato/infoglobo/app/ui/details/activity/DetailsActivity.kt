package com.developer.renato.infoglobo.app.ui.details.activity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.developer.renato.infoglobo.R
import com.developer.renato.infoglobo.app.ui.main.MainViewModel
import com.developer.renato.infoglobo.databinding.DetailsBinding
import com.developer.renato.infoglobo.domain.model.Conteudo
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat

class DetailsActivity: AppCompatActivity(){
    private val viewModel by viewModel<MainViewModel>()
    val binding by lazy {
        DataBindingUtil.setContentView<DetailsBinding>(this, R.layout.details)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(binding.toolbar)
        getSupportActionBar()?.setTitle(resources.getString(R.string.app_empyt))
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)

        binding.viewModel = viewModel
        var conteudo = Gson().fromJson(this.intent.extras?.get(resources.getString(R.string.key)) as String, Conteudo::class.java)
        binding.conteudo = conteudo

        conteudo.autores.let {
            if (it != null) {
                if (it.size>0){
                    binding.autor.text = "Por "+ it
                }
                binding.autor.text = resources.getString(R.string.app_empyt)
            }
        }

        var url: String =  ""
        if (conteudo.imagens!=null && conteudo.imagens!!.size>0){
            url = conteudo.imagens?.get(0)?.url.toString()
            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.imageholder)
                    .error(R.drawable.imageholder_error)
                    .into(binding.imageView)
        }
        conteudo.secao?.nome?.let {
            binding.textViewToolbar.text = it
        }
        binding.textViewDataHora.text =   formattedDate(conteudo.publicadoEm as String)


    }

    private fun observeSharedData() {
        viewModel.message.observe(
                this,
                Observer {data ->
                    var conteudo = Gson().fromJson(data as String, Conteudo::class.java)
                    binding.conteudo = conteudo
                })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun formattedDate(date: String): String{
        val parser =  SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val formatter = SimpleDateFormat("dd.MM.yyyy HH:mm")
        return  formatter.format(parser.parse(date))
    }
}