package com.developer.renato.infoglobo.resources.remote.api

import com.developer.renato.infoglobo.resources.remote.response.NoticiasResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface NoticiasApi {
    @GET("capa.json")
    fun getAll(): Deferred<List<NoticiasResponse>>
}