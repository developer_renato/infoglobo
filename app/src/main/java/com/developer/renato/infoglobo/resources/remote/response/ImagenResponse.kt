package com.developer.renato.infoglobo.resources.remote.response

import com.squareup.moshi.Json

class ImagenResponse {
    @Json(name = "autor")
    private val autor: String? = null

    @Json(name = "fonte")
    private val fonte: String? = null

    @Json(name = "legenda")
    private val legenda: String? = null

    @Json(name = "url")
    private val url: String? = null
}