package com.developer.renato.infoglobo

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.developer.renato.infoglobo.app.ui.main.activity.NoticiaActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NoticiaActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule(NoticiaActivity::class.java)

    @Test
    fun  appLaunchesSuccessfully () {
        ActivityScenario.launch (NoticiaActivity::class.java)
    }

    @Before
    fun setUp() {
    }

    @Test
    fun setView() {
        onView(withId(R.id.recyclerView)).perform(click())
        onView(withText(R.string.app_name_toolbar)).check(matches(isDisplayed()))
        onView(withId(R.id.recyclerView)).perform(click())
    }
}